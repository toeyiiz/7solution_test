package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func main() {
	choice := 0
	fmt.Printf("Enter your Test No. : ")
	fmt.Scanln(&choice)

	if choice == 1 {
		file, err := os.Open("hard.json")
		if err != nil {
			fmt.Println("Error opening file:", err)
			return
		}
		defer file.Close()
		decoder := json.NewDecoder(file)

		var data_arr [][]int

		if err := decoder.Decode(&data_arr); err != nil {
			fmt.Println("Error decoding JSON:", err)
			return
		}
		fmt.Println(ListSumValueobject(data_arr))
		main()
	} else if choice == 2 {
		convertencodetonumber()
		main()
	} else if choice == 3 {
		findbeef()
		main()
	} else {
		fmt.Println("End of Test.Thankyou")
	}

	//

	//
}

func ListSumValueobject(list_data [][]int) int {
	arr_map := make(map[[2]int]int)
	return findvalueIndex(list_data, 0, 0, arr_map)
}

func findvalueIndex(list_data [][]int, row, col int, arr_map map[[2]int]int) int {
	if row == len(list_data)-1 {
		return list_data[row][col]
	}

	if val, ok := arr_map[[2]int{row, col}]; ok {
		return val
	}

	leftPathSum := findvalueIndex(list_data, row+1, col, arr_map)
	rightPathSum := findvalueIndex(list_data, row+1, col+1, arr_map)
	result := list_data[row][col] + max(leftPathSum, rightPathSum)
	arr_map[[2]int{row, col}] = result
	return result
}

func convertencodetonumber() {
	start_number := 0
	encode_text := ""
	fmt.Printf("Input Encode : ")
	fmt.Scanln(&encode_text)
	encode_text = strings.ToUpper(encode_text)
	// fmt.Printf("Input Starter Number : ")
	// fmt.Scanln(&start_number)
	sizearr := len(encode_text) + 1
	result_number := make([]int, sizearr)

	var characters []string

	// Split the text by empty string to get individual characters
	for _, char := range encode_text {
		characters = append(characters, string(char))
	}

	for index, char := range characters {
		if char == "L" {
			if index == 0 {
				result_number[index] = start_number + 1
				result_number[index+1] = start_number
			} else {
				result_number[index] = result_number[index] + 1
				result_number[index+1] = start_number
				reverse_index := index - 1
				for i := reverse_index; i >= 0; i-- {
					if i < index && characters[i] == "R" {
						result_number[i+1] = result_number[i] + 2
						break
					}
					result_number[i] = result_number[i] + 1
				}
			}
		} else if char == "R" {
			if index == 0 {
				result_number[index] = start_number
				result_number[index+1] = start_number + 1
			} else {
				result_number[index+1] = result_number[index] + 1
			}
		} else if char == "=" {
			if index == 0 {
				result_number[index] = start_number
				result_number[index+1] = start_number
			} else {
				result_number[index+1] = result_number[index]
			}
		}
	}
	fmt.Println(characters)
	fmt.Println(result_number)
}

func findbeef() {
	url := "https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text"
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("Error making the HTTP request: %v\n", err)
		return
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("Error reading the response body: %v\n", err)
		return
	}
	text := string(body)

	substrings := strings.Fields(text)
	//fmt.Println(substrings)

	counts := make(map[string]int)

	for _, v := range substrings {
		beef_name := strings.Trim(v, ",. ")
		beef_name = strings.ToLower(beef_name)
		if beef_name != "" {
			counts[beef_name]++
		}
	}
	fmt.Println(counts)
}
